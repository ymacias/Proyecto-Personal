//get dependencies
const http       = require('http'),
      express    = require('express'),
      app        = express(),
      path       = require('path'),
      portalSession= require('cookie-session'),
      fs           = require('fs'),
      bodyParser = require('body-parser');

	  app.disable('x-powered-by');

var mailExp = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
var passExp = /^.{8,30}$/;
var dateExp = /[0-9][0-9]\/[0-9][0-9]\/[1-3][0-9][0-9][0-9]/;

app.use(bodyParser.json({type:'application/*',limit:'2mb'}));
app.use(bodyParser.urlencoded({extended:false}));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
//get our API routes
const api = require('./server/api');
//set our api routes
app.use('/api',api);
//parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));
//catch all other routes and return the index form-inline
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});
//get port from enviroment and store in express
const port = process.env.PORT || '3000';
app.set('port', port);
//create HTTP server
const server = http.createServer(app);
//Listen on provided port, on all network interfaces
server.listen(port, () => console.log(`API ruuning in local:${port}`));
